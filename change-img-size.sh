#!/bin/bash
scale=2
dataset_file="$1"
new_dataset_file="$(basename $dataset_file | sed 's/.xml//g')_$scale""Xscaled_down.xml"
echo -e "<?xml version='1.0' encoding='ISO-8859-1'?>
<?xml-stylesheet type='text/xsl' href='image_metadata_stylesheet.xsl'?>
<dataset>
<name>imglab dataset</name>
<comment>Created by imglab tool.</comment>
<images>" >> $new_dataset_file
dir=$scale"Xscaled_down"
if [ ! -d "$dir" ]; then 
    mkdir "$dir"
fi
for file in home-head-pose-dataset-imgs/*png
do
    size=$(file $file | cut -d ' ' -f 5)
    rect="$(cat "$dataset_file" | grep -Pzo "<image file.*$file.*\n.*")"
    if [ -z "$rect" ]; then
        continue
    fi
    new_size=$(($size/$scale))
    output="$dir/$(basename $file | sed s/.png//g)_$scale""Xscaled_down.png"
    convert $file -resize $new_size $output
    box=$(echo $rect | grep -o "top=.*'")
    top=$(echo $box | cut -d ' ' -f 1 | sed 's/[^0-9]*//g')
    left=$(echo $box | cut -d ' ' -f 2 | sed 's/[^0-9]*//g')
    width=$(echo $box | cut -d ' ' -f 3 | sed 's/[^0-9]*//g')
    height=$(echo $box | cut -d ' ' -f 4 | sed 's/[^0-9]*//g')
    label="$(cat $dataset_file | grep -Pzo "<image file.*$file.*\n.*\n.*")"
    label=$(echo "$label" | grep -o "<label>.*<" | sed 's/<label>//g' | sed 's/<//g')
    echo -e "\t<image file='$(realpath $output)'>" >> $new_dataset_file
    if [[ "$file" != *"empty"* ]]; then
        echo -e "\t\t<box top='$((top/scale))' left='$((left/scale))' width='$((width/scale))' height='$((height/scale))'>
        \t<label>$label</label>
        </box>" >> $new_dataset_file
    fi
    echo -e "\t</image>" >> $new_dataset_file
    
done
echo -e "</images>
</dataset>" >> $new_dataset_file
