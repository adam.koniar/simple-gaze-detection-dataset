#!/bin/bash
for template in templates/*.xml 
do
    dataset=$(echo $template | sed 's/\(templates\/\|_template\)//g')
    sed -e 's/{\$path}/'$(pwd | sed s+/+\\\\/+g)'/g' $template > $dataset
done