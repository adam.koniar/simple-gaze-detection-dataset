# Gaze detection dataset
This repository contains datasets created by me for my bachelor's thesis.

An Imglab is used for the creation of my dataset. The imglab is util of dlib.

## Setup
The Imglab and dlib works with absolute path of images so it's necessary to locate the datasets and run a script
```bash
$ ./generate_dataset.sh
```
which generate the dataset with absolute path to images from templates.

## Dataset description
The dataset have nine classes. Each class for one direction. The table above shows the map of label name and head direction 

head-f -> front

head-d -> down

head-t -> top

head-l -> left

head-r -> right

head-dl -> down left

head-dr -> down right

head-tr -> top right

head-tl -> top left


## Home head pose dataset

This is simple dataset created by web camera of the laptop.

Web camera attributes:
* Resolution: 1280/720
* fps: 30

## Creation description

For the research purpose is used the web camera of my laptop. (Web camera description is above this sub chapter)

1. The camera is put in front to me above 1 m and in angle above 25°.
2. I recorded one video for each head position plus for empty space
3. I split video into set of images
4. I mark head and set label

Graped data from image looks like how is shown in fig 1

Here will be awesome figure of one sample image from dataset


