#!/bin/bash

if [ -z $1 ]; then
    echo 'Define path to dataset'
    exit -1
fi

for attr in width height; do
    values=$(cat $1 | grep -Po $attr=\'[0-9]*\' | sed s/\'//g | sed s/$attr=//g)
    min=1000
    max=0
    sum=0
    count=$(echo -e $values | sed "s/ /\n/g" | wc -l)
    for n in $values; do 
        sum=$(($sum+$n))
        if [ $min -gt $n ]; then 
            min=$n 
        fi
        if [ $max -lt $n ]; then
            max=$n
        fi
    done
    echo "$attr max: $max min: $min avg: $(($sum/$count))"
done